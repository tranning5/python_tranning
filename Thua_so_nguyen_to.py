n = int(input("Nhập số n:"))
i = 2

while n > 1:
    if n % i == 0:
        print(i, end="")
        n = n // i
        if n > 1:
            print("*", end="")
    else:
        i += 1

