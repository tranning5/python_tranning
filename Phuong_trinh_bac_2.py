a = int(input("Nhập giá trị a:"))
b = int(input("Nhập giá trị b:"))
c = int(input("Nhập giá trị c"))
if a != 0:
    delta = (b ** 2) - (4 * a * c)
    if delta > 0:
        x1 = ((-b) + delta ** (1 / 2)) / (2 * a)
        x2 = ((-b) - delta ** (1 / 2)) / (2 * a)
        print("Phương trình có 2 nghiệm phân biệt: ", "x1 =", x1, "x2 =", x2)
    if delta == 0:
        print("Phương trình có nghiệm kép: x1 = x2 =", (-b)/(2*a))
    if delta < 0:
        print("Phương trình vô nghiệm")
else:
    print("Không thỏa mãn điều kiện phương trình bậc 2 a khác 0")
